import React from 'react';
import './GenerateJoke.css';

const GenerateJoke = props => {
  return (
    <button className='GenerateJoke-btn'
      onClick={props.generate}
    >
      Generate
    </button>
  );
};

export default GenerateJoke;
