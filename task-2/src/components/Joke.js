import React from 'react';
import './Joke.css';

const Joke = props => {
  return (
    <div className='Joke'>
      <p style={{fontWeight: 'bold'}}>Chuck Norris joke</p>
      <p>{props.joke}</p>
      <img src={props.icon} alt=""/>
    </div>
  );
};

export default Joke;