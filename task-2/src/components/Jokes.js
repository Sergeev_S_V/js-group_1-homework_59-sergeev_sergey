import React from 'react';
import Joke from "./Joke";
import './Jokes.css';

const Jokes = props => {
  return (
    <div className='Jokes'>
      {props.jokes.map((jokes, index) => {
        return (
          <Joke joke={jokes.value}
                icon={jokes.icon_url}
                key={index}/>
        );
      })}
    </div>
  );
};

export default Jokes;