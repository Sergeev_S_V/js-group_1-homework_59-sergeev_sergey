import React, { Component } from 'react';
import './App.css';
import Jokes from "../components/Jokes";
import GenerateJoke from "../components/Button/GenerateJoke";

class App extends Component {

  state = {
    jokes: []
  };

  generateFiveJokes = () => {
    const getOneJoke = 'https://api.chucknorris.io/jokes/random';
    let promiseAll = [];
    for (let i = 0; i < 5; i++) {
      promiseAll.push(fetch(getOneJoke).then(response => response.json()));
    }
    return Promise.all(promiseAll);
  };


  getFewJokes = () => {
    this.generateFiveJokes().then(jokes => {
      this.setState({jokes});
    })
  };

  componentDidMount() {
    this.getFewJokes()
  }

  render() {
    return (
      <div className="App">
        <GenerateJoke generate={this.getFewJokes}/>
        <Jokes jokes={this.state.jokes}/>
      </div>
    );
  }
}

export default App;
